package com.favian.loginapp;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    TextView loggedEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loggedEmail = findViewById(R.id.loggedEmail);

        String stringExtra = getIntent().getExtras().getString("LOGGED_EMAIL");
        loggedEmail.setText("Logged as " + stringExtra);

        imageView = findViewById(R.id.imageView);
        draw();
    }

    private void draw() {
        Bitmap blankBitmap = Bitmap.createBitmap(600, 600, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(blankBitmap);
        imageView.setImageBitmap(blankBitmap);
        Paint paint = new Paint();
        canvas.drawColor(Color.argb(255,255,255,255));
        paint.setColor(Color.argb(255,0,100,0));
        canvas.drawText("Holla Theo!!", 250,570, paint);
        drawTriangle(canvas, paint, 300, 150, 310);
        paint.setColor(Color.argb(255,0,255,0));
        canvas.drawRect(170,305,430,550,paint);
        paint.setColor(Color.argb(255,255,255,255));
        canvas.drawCircle(300, 240, 50, paint);
    }

    public void drawTriangle(Canvas canvas, Paint paint, int x, int y, int width) {
        int halfWidth = width / 2;

        Path path = new Path();
        path.moveTo(x, y - halfWidth); // Top
        path.lineTo(x - halfWidth, y + halfWidth); // Bottom left
        path.lineTo(x + halfWidth, y + halfWidth); // Bottom right
        path.lineTo(x, y - halfWidth); // Back to Top
        path.close();

        canvas.drawPath(path, paint);
    }
}
