package com.favian.loginapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    final RegisterActivity CONTEXT = RegisterActivity.this;
    public static String EXTRA_SELECTED_VALUE = "extra_selected_value";
    public static int RESULT_CODE = 110;

    TextView backLogin, doSignup;
    EditText regName, regPhone, regEmail, regPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getSupportActionBar().setTitle("Sign Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        backLogin = findViewById(R.id.backLogin);
        backLogin.setOnClickListener(this);

        regName = findViewById(R.id.regName);
        regPhone = findViewById(R.id.regPhone);
        regEmail = findViewById(R.id.regEmail);
        regPassword = findViewById(R.id.regPassword);

        doSignup = findViewById(R.id.doSignup);
        doSignup.setOnClickListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backLogin:
                onBackPressed();
                break;
            case R.id.doSignup:
                Toast.makeText(CONTEXT, "Register Success", Toast.LENGTH_SHORT).show();
                Intent resultIntent = new Intent();
                resultIntent.putExtra(EXTRA_SELECTED_VALUE, regEmail.getText().toString().trim());
                setResult(RESULT_CODE, resultIntent);
                finish();
        }
    }
}
